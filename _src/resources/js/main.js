// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {

  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 

    },
    finalize: function() {

    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 

    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here

    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;

    // hit up common first.
    UTIL.fire( 'common' );

    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);

/* Youtube API */

var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubePlayerAPIReady() {
  var $m = $("#movie_player");
  player = new YT.Player('movie_player', {
    playerVars: { 'autoplay': 1, 'controls': 0,'autohide':1,'wmode':'opaque', 'loop': 1, 'rel':0, 'showinfo':0, 'fs':0,'playlist':$m.data('video') },
    videoId: $m.data("video"),
    events: {
      'onReady': onPlayerReady}
    });
}

// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();
console.log(navbarHeight);

$(window).scroll(function(event){
  didScroll = true;
});

setInterval(function() {
  if (didScroll) {
    hasScrolled();
    didScroll = false;
  }
}, 250);

function hasScrolled() {
  var st = $(this).scrollTop();
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
      return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('nav-down').addClass('nav-up').css('top',-navbarHeight);
      } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
          $('header').removeClass('nav-up').addClass('nav-down').css('top','0');
        }
      }

      lastScrollTop = st;
    }

    /* Flex Destroy */
    function flexdestroy(selector) {
        var el = $(selector);
        var elClean = el.clone();

        elClean.find('.flex-viewport').children().unwrap();
        elClean
            .removeClass('flexslider')
            .find('.clone, .flex-direction-nav, .flex-control-nav')
            .remove()
            .end()
            .find('*').removeAttr('style').removeClass(function (index, css) {
                return (css.match(/\bflex\S+/g) || []).join(' ');
            });

        elClean.insertBefore(el);
        el.remove();
    }

    /* Velocity Page Load Animation */

/*    var isInView = function ($element) {
        var win = $(window);
        var obj = $element;
        var scrollPosition = win.scrollTop();
        var visibleArea = win.scrollTop() + win.height() + obj.data('offset') + 200;
        var objEndPos = (obj.offset().top + obj.outerHeight());

        return (visibleArea >= objEndPos && scrollPosition <= objEndPos ? true : false)
    };


    $('section, footer .footer-contain').not('.hero').each(function(){
      var offset = $(this).outerHeight() * 0.875;
      $(this).addClass('fade').attr('data-offset',offset);
    });

    $('.item').addClass('content-animate');


    $(window).ready(function(){
      $('section:first').addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
         stagger:150,
         delay:500,
         visibility:'visible'
      });
    })




    $(window).on('scroll load', function () {
        $('section,footer .footer-contain').each(function (key, val) {
            var $el = $(this);

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

        });
    });*/

    $( "iframe" ).wrap( "<div class='video-container'></div>" );


/*
  Set Session Cookie
  */


/*  if ($.cookie('visited') != 'true'){
    $('#popup').modal('show');
    $.cookie('visited', 'true', { expires: 15 });
  }

  $('.playVideo').click(function(){
    $('.content-container').velocity("fadeOut", { duration: 1000 })
    $('.video-wrapper').velocity("fadeIn", {  delay: 999, duration: 1000 })
  })
*/

  /* Init */
  $(".fancybox").fancybox();

  $(".gallery li").lazyload({
    effect : "fadeIn"
  });


$(document).ready(function(){
  $("#mobile-menu").menumaker({
      format: "multitoggle"
    });
    $('li.has-sub > a').attr('href',"");
})